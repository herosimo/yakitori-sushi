import $, { htmlPrefilter, ajax } from "jquery";
window.$ = window.jQuery = $;

import "slick-carousel";
import "fslightbox";

(function ($) {
	// CEW Slider Posts
	$(".sliderPosts").slick({
		// accessibility: false,
		// dots: true,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		prevArrow: $(".sliderPosts-arrow-left"),
		nextArrow: $(".sliderPosts-arrow-right"),
		responsive: [
			{
				breakpoint: 1025,
				settings: {
					centerMode: true,
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: "0px",
					slidesToShow: 1,
				},
			},
		],
	});

	// FS Lightbox trigger
	$(".fslightbox-trigger img").wrap(function () {
		return `<a data-fslightbox="gallery" href="${this.src}"></a>`;
	});

	refreshFsLightbox();
})(jQuery);
