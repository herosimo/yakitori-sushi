<?php

/*CPT Jobs*/
add_action("init", "jobs_post_types");

function jobs_post_types()
{
	$labels = [
		"name" => _x("Jobs", "Jobs General Name", "hello-elementor"),
		"singular_name" => _x("Job", "Post Type Singular Name", "hello-elementor"),
		"menu_name" => __("Jobs", "hello-elementor"),
		"name_admin_bar" => __("Job", "hello-elementor"),
		"archives" => __("Job Archives", "hello-elementor"),
		"attributes" => __("Job Attributes", "hello-elementor"),
		"parent_item_colon" => __("Parent Jobs:", "hello-elementor"),
		"all_items" => __("All Jobs", "hello-elementor"),
		"add_new_item" => __("Add New Job", "hello-elementor"),
		"add_new" => __("Add New", "hello-elementor"),
		"new_item" => __("New Job", "hello-elementor"),
		"edit_item" => __("Edit Job", "hello-elementor"),
		"update_item" => __("Update Job", "hello-elementor"),
		"view_item" => __("View Job", "hello-elementor"),
		"view_items" => __("View Jobs", "hello-elementor"),
		"search_items" => __("Search Jobs", "hello-elementor"),
		"not_found" => __("Not found", "hello-elementor"),
		"not_found_in_trash" => __("Not found in Trash", "hello-elementor"),
		"featured_image" => __("Featured Image", "hello-elementor"),
		"set_featured_image" => __("Set featured image", "hello-elementor"),
		"remove_featured_image" => __("Remove featured image", "hello-elementor"),
		"use_featured_image" => __("Use as featured image", "hello-elementor"),
		"insert_into_item" => __("Insert into item", "hello-elementor"),
		"uploaded_to_this_item" => __("Uploaded to this Job", "hello-elementor"),
		"items_list" => __("Jobs list", "hello-elementor"),
		"items_list_navigation" => __("Jobs list navigation", "hello-elementor"),
		"filter_items_list" => __("Filter jobs list", "hello-elementor"),
	];
	$args = [
		"label" => __("Jobs", "hello-elementor"),
		"description" => __("Job Description", "hello-elementor"),
		"labels" => $labels,
		"supports" => ["title", "thumbnail", "editor"],
		"hierarchical" => false,
		"public" => true,
		"show_in_rest" => true, //to show in  gutenberg
		"show_ui" => true,
		"show_in_menu" => true,
		"menu_position" => 5,
		"menu_icon" => "dashicons-businesswoman",
		"show_in_admin_bar" => true,
		"show_in_nav_menus" => true,
		"can_export" => true,
		"has_archive" => true,
		"exclude_from_search" => false,
		"publicly_queryable" => true,
		"capability_type" => "page",
		"rewrite" => [
			"slug" => __("jobs", "hello-elementor"),
		],
	];
	register_post_type("jobs", $args);
}
/*End CPT Jobs*/
