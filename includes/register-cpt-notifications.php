<?php

/*CPT Notifications*/
add_action("init", "notifications_post_types");

function notifications_post_types()
{
	$labels = [
		"name" => _x("Notifications", "notification General Name", "hello-elementor"),
		"singular_name" => _x("Notification", "Post Type Singular Name", "hello-elementor"),
		"menu_name" => __("Notifications", "hello-elementor"),
		"name_admin_bar" => __("Notification", "hello-elementor"),
		"archives" => __("Notification Archives", "hello-elementor"),
		"attributes" => __("Notification Attributes", "hello-elementor"),
		"parent_item_colon" => __("Parent Notifications:", "hello-elementor"),
		"all_items" => __("All Notifications", "hello-elementor"),
		"add_new_item" => __("Add New Notification", "hello-elementor"),
		"add_new" => __("Add New", "hello-elementor"),
		"new_item" => __("New Notification", "hello-elementor"),
		"edit_item" => __("Edit Notification", "hello-elementor"),
		"update_item" => __("Update Notification", "hello-elementor"),
		"view_item" => __("View Notification", "hello-elementor"),
		"view_items" => __("View Notifications", "hello-elementor"),
		"search_items" => __("Search Notifications", "hello-elementor"),
		"not_found" => __("Not found", "hello-elementor"),
		"not_found_in_trash" => __("Not found in Trash", "hello-elementor"),
		"featured_image" => __("Featured Image", "hello-elementor"),
		"set_featured_image" => __("Set featured image", "hello-elementor"),
		"remove_featured_image" => __("Remove featured image", "hello-elementor"),
		"use_featured_image" => __("Use as featured image", "hello-elementor"),
		"insert_into_item" => __("Insert into item", "hello-elementor"),
		"uploaded_to_this_item" => __("Uploaded to this notification", "hello-elementor"),
		"items_list" => __("Notification list", "hello-elementor"),
		"items_list_navigation" => __("Notification list navigation", "hello-elementor"),
		"filter_items_list" => __("Filter notification list", "hello-elementor"),
	];
	$args = [
		"label" => __("Notifications", "hello-elementor"),
		"description" => __("Notification Description", "hello-elementor"),
		"labels" => $labels,
		"supports" => ["title", "editor", "thumbnail"],
		"hierarchical" => false,
		"public" => true,
		"show_in_rest" => true, //to show in  gutenberg
		"show_ui" => true,
		"show_in_menu" => true,
		"menu_position" => 5,
		"show_in_admin_bar" => true,
		"show_in_nav_menus" => true,
		"can_export" => true,
		"has_archive" => false,
		"menu_icon" => "dashicons-pressthis",
		"exclude_from_search" => false,
		"publicly_queryable" => false,
		"capability_type" => "page",
		"rewrite" => [
			"slug" => __("notifications", "hello-elementor"),
		],
	];
	register_post_type("notifications", $args);
}
/*End CPT Notifications*/
