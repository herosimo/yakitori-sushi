<?php
namespace Elementor;

class get_sliderPosts extends Widget_Base
{
	public function get_name()
	{
		return "cew-slider-posts";
	}

	public function get_title()
	{
		return "CEW Slider Posts";
	}

	public function get_icon()
	{
		return "eicon-posts-carousel";
	}

	public function get_categories()
	{
		return ["basic"];
	}

	protected function _register_controls()
	{
		$this->start_controls_section("content_section", [
			"label" => __("Content", "hello-elementor"),
			"tab" => \Elementor\Controls_Manager::TAB_CONTENT,
		]);

		$this->add_control("items", [
			"label" => __("Items", "hello-elementor"),
			"type" => \Elementor\Controls_Manager::NUMBER,
			"min" => 1,
			"max" => 10,
			"step" => 1,
			"default" => 3,
		]);

		$this->add_control("no_post_msg", [
			"label" => __("No Post Message", "hello-elementor"),
			"type" => \Elementor\Controls_Manager::TEXT,
			"default" => __("Er is geen bericht", "hello-elementor"),
			"placeholder" => __("No Post Message", "hello-elementor"),
		]);

		$this->end_controls_section();
	}

	protected function render()
	{
		$settings = $this->get_settings_for_display();
		$items = $settings["items"];
		$noPostMsg = $settings["no_post_msg"];
		?>

        <section id="sliderPosts">
             <!-- Slider items -->
            <div class="sliderPosts">
            <?php
            $args = [
            	"posts_per_page" => $items,
            	"orderby" => "date",
            	"order" => "DESC",
            ];

            $latest_posts = get_posts($args);
            if ($latest_posts):
            	foreach ($latest_posts as $post) { ?>
                <article>
                    <a href="<?php echo get_the_permalink($post->ID); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt=""  />
                    </a>
                    <a href="<?php echo get_the_permalink($post->ID); ?>">
                        <h3><?php echo $post->post_title; ?></h3>
                    </a>
                    <p><?php echo wp_trim_words(get_the_content($post->ID), 25, " ..."); ?></p>
                    <a href="<?php echo get_the_permalink($post->ID); ?>" class="read-more">Lees verder</a>
                </article>
                <?php }
            else:
            	 ?>
                <p class="no-post"><?php echo $noPostMsg; ?></p>
            <?php
            endif;
            ?>
            </div>

            <!--Arrow navigation -->
            <div class="slider-arrow">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/slider-left.svg" alt="" class="sliderPosts-arrow-left" />
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/slider-right.svg" alt="" class="sliderPosts-arrow-right" />
            </div>
        </section>

    <?php
	}
	protected function _content_template()
	{
	}
}
