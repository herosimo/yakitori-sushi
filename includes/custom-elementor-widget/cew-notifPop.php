<?php
namespace Elementor;

class get_notifPop extends Widget_Base
{
	public function get_name()
	{
		return "cew-notif-pop";
	}

	public function get_title()
	{
		return "CEW Notification Popup";
	}

	public function get_icon()
	{
		return "eicon-products";
	}

	public function get_keywords()
	{
		return ["archive", "popup", "notification"];
	}

	public function get_categories()
	{
		return ["basic"];
	}

	protected function _register_controls()
	{
	}

	protected function render()
	{
		$notifications = get_posts(["post_type" => "notifications", "post_status" => "publish"]);
		if (!empty($notifications)) { ?>

    <?php foreach ($notifications as $notification):
    	$date_start = get_field("start_date", $notification->ID);
    	$date_end = get_field("end_date", $notification->ID);
    	$now = date("Y-m-d", strtotime("now"));
    	$title = $notification->post_title;
    	$desc = get_field("desc_nf", $notification->ID);

    	if ($now >= $date_start && $now <= $date_end): ?>
	
	<div class="notification-popup" style="border-bottom: 1px solid #57575733; margin-bottom: 16px;">
	    <p class="notification-popup__title" style="font-family: var(--e-global-typography-primary-font-family); font-size: 16px; font-weight: var(--e-global-typography-primary-font-weight); margin-bottom: 8px; color:var( --e-global-color-secondary );"><?php echo $title; ?></p>
	    <p clas="notification-popup__body" style="font-family: var(--e-global-typography-text-font-family); font-weight: var(--e-global-typography-text-font-weight); font-size: 14px; color: var( --e-global-color-text ); margin-bottom: 16px;"><?php echo $desc; ?></p>
	</div>
	
    <?php endif;
    endforeach; ?>
<?php }
	}
	protected function _content_template()
	{
	}
}
