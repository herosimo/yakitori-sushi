<?php
/**
 * Display Pop Up Notifications TOASTS
 *
 */

function display_notification_popup()
{
	$hasNotification = get_posts(["post_type" => "notification"]);
	$hasJob = get_posts([
		"post_type" => "jobs",
		"post_status" => "publish",
	]);
	?>

    <div id="toasts">

        <?php
        if ($hasNotification) {
        	foreach ($hasNotification as $notif) {
        		if (!isset($_COOKIE["notification_" . $notif->ID])) {
        			// Notifications Variables
        			$date_start = strtotime(get_field("start_date", $notif->ID));
        			$date_end = strtotime(get_field("end_date", $notif->ID));
        			$now = strtotime("now");
        			$title = $notif->post_title;
        			$desc = get_field("desc_nf", $notif->ID);

        			if ($now >= $date_start && $now <= $date_end) { ?>
                        <div class="toast toast_<?php $notif->ID; ?>" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" data-notification="notification_<?php echo $notif->ID; ?>">
                            <div class="toast-header">
                                <strong class="mr-auto"><?php echo $title; ?></strong>
                                <button id="button_<?php $notif->ID; ?>" type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">
                                <?php echo $desc; ?>
                            </div>
                        </div>
                    <?php }
        		}
        	}
        }

        //Job Variables
        $title = get_field("job_title", "options");
        $desc = get_field("job_desc", "options");
        // $job_link = get_field('job_popup_rf', 'options');

        if ($hasJob && $desc) {
        	if (!isset($_COOKIE["notification_jobNotif"])) { ?>
                <div class="toast toast_jobNotif" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" data-notification="notification_jobNotif">
                    <div class="toast-header">
                        <strong class="mr-auto"><?php echo $title; ?></strong>
                        <button id="button_jobNotif" type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        <p><?php echo $desc; ?></p>
                        <p class="mb-0">
                            <a href="<?php echo get_site_url() . "/jobs"; ?>" class="font-weight-bold close">
                                <?php echo esc_html__("Bekijk vacatures", "biereindhoven") .
                                	" (" .
                                	count($hasJob) .
                                	")"; ?>
                            </a>
                        </p>
                    </div>
                </div>
        <?php }
        }?>

    </div>
    
<?php
}
