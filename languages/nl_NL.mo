��    w      �  �   �      
     
     ,
     4
     @
     U
     ^
     p
  
   �
     �
  
   �
     �
  E   �
     �
     �
     
       	         *     8  C   F  ,   �  1   �  ,   �  1     A   H     �     �     �     �     �     �     �  
   �     �     �       B     ?   V  L   �  M   �     1  i   M  �   �     F  
   K     V     g     ~     �  1   �  /   �     �                    !     1  	   6     @     U     \     w  
   |     �     �     �     �     �     �  	   �     �     �     �               7     I     f     t     �     �     �     �     �     �     �             	   %  
   /     :     B  
   J  "   U     x      �     �     �     �  
   �     �  
   �     �  E        Q     f     �  z   �       	        (     :     M  L   S  S   �     �     �              #     6     F     `     y     �     �     �     �  	   �     �  W   �     3     9     Q     X  	   g     q     �  G   �  1   �  9   
  0   D  8   u  B   �  	   �     �               )     @     W     l     ~     �     �  J   �  K   �  U   5  O   �     �  w   �  �   q  	                  .     G  	   \  8   f  5   �  	   �     �     �     �       	   !  
   +     6     Q     Y     x     }     �     �     �     �     �     �     �     �     �                &     ;     L  	   j     t     �     �  "   �     �     �     �     �         
   7   	   B   
   L      W      _      f   -   r   #   �      �      �      �      �   
   !     !     !!     4!  Q   C!     �!     �!  "   �!  �   �!     x"     �"     �"     �"     �"  L   �"  Z   #  	   n#  	   x#     �#     u          6                      A   L       ;       T   N       H   f   o   Y   >   %       !       a   8                           G       I   *   Z   7   0       /          `              @   j          v   
   w   M      K   J   9   i   .       B   p       "   S   s   3   ^      (   h             k   D          g   U      )       R              W                      <       -          O           $   Q      d           l   n   '   ,   #   +   _   q       e   2         :   V   1       F   m          [   P   5      ?       &   	   =          b      ]          4   X   E       r       C   c   \   t              Activate Elementor Add New Add New Job Add New Notification All Jobs All Notifications All rights reserved Background Boxed Breakpoint Centered Changes will be reflected in the preview only after the page reloads. Color Comments are closed. Content Content Width Copyright Create Footer Create Header Create a cross-site Header and Footer using Elementor & Hello theme Create a custom footer with multiple options Create a custom footer with the new Theme Builder Create a custom header with multiple options Create a custom header with the new Theme Builder Create cross-site Header and Footer using Elementor & Hello theme Default Dropdown Edit Job Edit Notification Featured Image Filter jobs list Filter notification list Full Width Gap Get Elementor Go Pro Go to <a href="%s" target="_blank">Menus screen</a> to create one. Go to <a href="%s">Site Identity</a> to manage your site's logo Go to <a href="%s">Site Identity</a> to manage your site's title and tagline Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus. Hello Theme Header & Footer Hello theme is a lightweight starter theme designed to work perfectly with Elementor Page Builder plugin. Hello theme is a lightweight starter theme. We recommend you use it together with Elementor Page Builder plugin, they work perfectly together! Hide Horizontal Insert into item Install &amp; Activate Install Elementor Inverted It looks like nothing was found at this location. It seems we can't find what you're looking for. Items Job Job Archives Job Attributes Job Description Jobs Jobs list Jobs list navigation Layout Learn more about Elementor Logo Logo Width Menu Menu Layout New Job New Notification No Post Message None Not found Not found in Trash Notification Notification Archives Notification Attributes Notification Description Notification list Notification list navigation Notifications One Response Parent Jobs: Parent Notifications: Remove featured image Search Jobs Search Notifications Search results for:  Set Your Header &amp; Footer Set featured image Show Site Logo Start Here Tagged  Tagline Text Color Thanks for installing Hello Theme! The page can&rsquo;t be found. There are no menus in your site. Title Toggle Color Type Typography Update Elementor Update Job Update Notification Upgrade to Elementor Pro and enjoy free design and many more features Uploaded to this Job Uploaded to this notification Use as featured image Use this experiment to design header and footer using Elementor Site Settings. <a href="%s" target="_blank">Learn More</a> View Job View Jobs View Notification View Notifications Width With the new Theme Builder you can jump directly into each part of your site You need Elementor version 3.1.0 or above to create a cross-site Header and Footer. jobs notifications — Select a Menu — Project-Id-Version: Elementor Wappstars Starter Theme
PO-Revision-Date: 2021-10-06 17:25+0700
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_x;esc_html_e;esc_html_x
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: dist
X-Poedit-SearchPathExcluded-1: node_modules
X-Poedit-SearchPathExcluded-2: export
 Activeer Elementor Nieuw Toevoegen Nieuwe vacature toevoegen Nieuwe melding toevoegen Alle vacatures Alle meldingen Alle rechten voorbehouden Achtergrond Verpakt Breekpunt Gecentreerd Wijzigingen worden pas in het voorbeeld weergegeven nadat de pagina opnieuw is geladen. Kleur Reacties zijn gesloten. Inhoud Inhoudsbreedte Copyright Voettekst maken Koptekst maken Maak een cross-site kop- en voettekst met het Elementor- en Hello-thema Maak een aangepaste voettekst met meerdere opties Maak een aangepaste voettekst met de nieuwe Theme Builder Maak een aangepaste koptekst met meerdere opties Maak een aangepaste koptekst met de nieuwe Theme Builder Maak een cross-site kop- en voettekst met Elementor en Hallo thema Standaard Laten vallen Vacature bewerken Bewerk melding Uitgelichte afbeelding Vacaturelijst filteren Filter meldingslijst Volledige breedte Gat Elementor downloaden Go Pro Ga naar <a href="%s" target="_blank">Menu's scherm</a> om er een te maken. Ga naar <a href="%s">Site-identiteit</a> om het logo van uw site te beheren Ga naar <a href="%s">Site-identiteit</a> om de titel en slogan van uw site te beheren Ga naar <a href="%s" target="_blank">Menu's scherm</a> om uw menu's te beheren. Hallo thema kop- en voettekst Hallo-thema is een lichtgewicht startthema dat is ontworpen om perfect te werken met de Elementor Page Builder-plug-in. Hallo-thema is een lichtgewicht startthema. We raden je aan om het samen met de Elementor Page Builder-plug-in te gebruiken, ze werken perfect samen! Verbergen Horizontaal In item invoegen Installeren en activeren Installeer Elementor Omgekeerd Het lijkt erop dat er op deze locatie niets is gevonden. Het lijkt erop dat we niet kunnen vinden wat u zoekt. Artikelen Vacature Vacature archief Vacaturekenmerken Vacature omschrijving Vacatures Jobs lijst Navigatie in de banenlijst Lay-out Meer informatie over Elementor Logo Logo Breedte Menu Menu-indeling Nieuwe vacature Nieuwe melding Geen bericht Geen Niet gevonden Niet gevonden in prullenbak Melding Notificatie archief Meldingskenmerken Meldingsbeschrijving Notificatielijst Navigatie lijst met meldingen Meldingen Een Antwoord Ouder vacature: Oudermeldingen: Uitgelichte afbeelding verwijderen Vacatures zoeken Zoekmeldingen Zoekresultaten voor:  Stel uw kop- en voettekst in Uitgelichte afbeelding instellen Laten zien Site-logo Begin hier Getagd  Slogan Tekst kleur Bedankt voor het installeren van Hallo-thema! De pagina kan niet worden gevonden. Er zijn geen menu's op uw site. Titel Wisselen van kleur Type Typografie Elementor bijwerken Vacature bijwerken Update melding Upgrade naar Elementor Pro en geniet van gratis ontwerp en nog veel meer functies Geüpload naar deze vacature Geüpload naar deze melding Gebruik als uitgelichte afbeelding Gebruik dit experiment om kop- en voetteksten te ontwerpen met Elementor-site-instellingen. <a href="%s" target="_blank">Leer meer</a> Bekijk vacature Bekijk vacatures Bekijk de melding Meldingen bekijken Breedte Met de nieuwe Theme Builder kun je direct naar elk deel van je site springen U hebt Elementor versie 3.1.0 of hoger nodig om een cross-site kop- en voettekst te maken. vacatures meldingen — Selecteer een menu — 